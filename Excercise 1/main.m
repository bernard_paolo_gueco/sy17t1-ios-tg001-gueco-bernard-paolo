//
//  main.m
//  Excercise 1
//
//  Created by TAFT-CL4&5 on 9/8/16.
//  Copyright © 2016 TAFT-CL4&5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Character.h"

// return_type function_name(param1, param2)
// void attack(Character* target)

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Character me = new Character();
        Character *me = [[Character alloc] initWithName:@"player1" level:1];
        Character *enemy = [[Character alloc] initWithName:@"player2" level:2];
        
        // me.attack(enemy, 10)
        [me attack:enemy];
        [enemy attack:me];
        
     //   BOOL fight = true;
    //    while (fight!) {
//
    //    }
    }
    return 0;
}
