//
//  Character.h
//  Excercise 1
//
//  Created by TAFT-CL4&5 on 9/8/16.
//  Copyright © 2016 TAFT-CL4&5. All rights reserved.
//

#ifndef Character_h
#define Character_h

#import <Foundation/Foundation.h>
/*
 using System;
 
 public class Character
 {
 public string Name { get; set; }
 
 public void Attack(Character target)
 {
 
 }
 
 public Character(string name)
 {
 Name = name;
 }
 }
 
 void main()
 {
 Character me = new Character("Kevin");
 }
 */

@interface Character : NSObject

@property (nonatomic, strong)NSString* name;
@property (nonatomic, assign)int hp;
@property (nonatomic, assign)int power;
@property (nonatomic, assign)int defense;
@property (nonatomic, assign)int level;

- (id)initWithName:(NSString*)name level:(int)level;
- (void)attack:(Character*)target;
- (void)setHp:(int)hp;
- (void)setPower:(int)power;
- (int)Fight;


@end

#endif /* Character_h */