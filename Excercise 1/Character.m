//
//  Character.m
//  Excercise 1
//
//  Created by TAFT-CL4&5 on 9/8/16.
//  Copyright © 2016 TAFT-CL4&5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Character.h"

@implementation Character

-(id)initWithName:(NSString *)name level:(int)level
{
    self = [super init];
    if (self)
    {
        self.name = name;
        self.level = level;
        self.hp = 100.00;
        self.power = 10;
        self.defense = 5;
    }
    
    return self;
}

//-(void) setHp:(int)hp
//{}



-(void)attack:(Character *)target
{
    
    NSLog(@"Attacking target: %@", [target name]);
    NSLog(@"Target HP: %d", [target hp]);
}

@end
