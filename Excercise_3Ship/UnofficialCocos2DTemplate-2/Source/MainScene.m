#import "MainScene.h"
#define ARC4RANDOM_MAX 0x100000000

@implementation MainScene

int asteroidCount =0;




-(instancetype)init
{
 
    
        if((self = [super init]))
        {
            self.color = [CCColor blackColor];
            
            CCSprite *ship = [CCSprite spriteWithImageNamed:@"playerShip1_blue.png"];
            [self addChild:ship];
            ship.positionType = CCPositionTypeNormalized;
            ship.position = ccp(0.5f, 0.5f);
   
            
        }
	
	return self;
}



-(void) update:(CCTime)delta
{

    
    if (asteroidCount<20)
    {
        
        double randNumA = ((double)arc4random() / ARC4RANDOM_MAX);
        double randNumB = ((double)arc4random() / ARC4RANDOM_MAX);
        
     
        CCActionRotateBy *rot = [CCActionRotateBy actionWithDuration:5 angle:360.0f];
            CCSprite *sprite = [CCSprite
                                spriteWithImageNamed:@"meteorBrown_small1.png"];
            
            [self addChild:sprite];
            sprite.positionType = CCPositionTypeNormalized;
            sprite.position = ccp(randNumA,randNumB);
        
        [sprite runAction:[CCActionRepeatForever actionWithAction:rot]];
        

        asteroidCount++;
    }
    
    else if (asteroidCount>20)
    {}
    
    
    
}

@end
