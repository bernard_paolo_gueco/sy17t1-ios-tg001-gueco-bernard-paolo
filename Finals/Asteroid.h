

#ifndef Asteroid_h
#define Asteroid_h

#import <Foundation/Foundation.h>

@interface Asteroid : CCSprite

-(id)initWithFile:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;

@end


#endif /* Asteroid_h */
