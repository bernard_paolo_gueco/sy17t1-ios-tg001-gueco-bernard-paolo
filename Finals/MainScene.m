#import "MainScene.h"
@implementation MainScene
float spawnTimer = 4.0f;
float spawnTimerTwo = 1.5f;
float spawnTimeElapsed = 0.0f;
float spawnTimeElapsedTwo = 0.0f;
// Declare a physics world
CCPhysicsNode *_physicsWorld;

float playerSpeed = 200.0f ;
float gameSpeed = 200.0f ;
int distance = 1 ;
bool gamePaused = true;
CCLabelTTF *_label; // time
CCLabelTTF *_gameover; // gameover

-(instancetype)init
{
    if((self = [super init]))
    {
        self.userInteractionEnabled = true;
        
        // Seed random number
        //srand(time(NULL));
        
        // Setup player
        self.player = [[Player alloc]initWithFile:@"scottpilgrim_multiple.png" andSpeed:playerSpeed];
        self.player.position = ccp(200, 50);
        [self.player setScale:0.5f];
        
        // Setup physics world
        _physicsWorld = [CCPhysicsNode node];
        _physicsWorld.gravity = ccp(0,0);
        _physicsWorld.collisionDelegate = self;
        [self addChild:_physicsWorld];
        
        
        
        self.player.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){CGPointZero, self.player.contentSize} cornerRadius:0];
        
        self.player.physicsBody.collisionGroup = @"playerGroup";
        self.player.physicsBody.collisionType = @"playerCollision";
        [_physicsWorld addChild:self.player];
        
        _label = [CCLabelTTF labelWithString:@"Score" fontName:@"Consolas" fontSize:12.0f];
        _label.position = ccp(50,300);
        [self addChild:_label];
        
        
       
    }
    
    return self;
}

-(void)update:(CCTime)delta
{
    spawnTimeElapsed += delta;
    spawnTimeElapsedTwo += delta;
    if (spawnTimeElapsed >= spawnTimer)
    {
        [self spawnAsteroid];
        spawnTimeElapsed = 0.0f;
    }
    
    if(spawnTimeElapsedTwo >= spawnTimerTwo)
    {
        [self spawnAsteroidTwo];
        spawnTimeElapsedTwo = 0.0f;
    }
    
    //So that the obstacles wont appear at the same interval
    if(spawnTimeElapsed == spawnTimeElapsedTwo)
    {
        spawnTimeElapsed+=1;
    }
    
    distance++;
    // How to change label
    [_label setString:[NSString stringWithFormat: @"%d", distance]];
}


- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [self movePlayer:touch];
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [self movePlayer:touch];
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    self.player.isMoving = false;
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair asteroidCollision:(CCNode *)asteroid playerCollision:(CCNode *)player
{
    [[CCDirector sharedDirector] pause];
    [player removeFromParentAndCleanup:YES];
    return YES;
}

- (void)movePlayer:(CCTouch *)touch
{
    // Move the player
 
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    if (touch.locationInWorld.y < viewSize.height/2)
    {
        [self.player setPosition:ccp(200,50)];
        
    }
    else
    {
        [self.player setPosition:ccp(200, 250)];
    }
    self.player.isMoving = true;
}

- (void)spawnAsteroid
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    // Store random asteroid file names
    NSMutableArray *asteroidFiles = [NSMutableArray arrayWithObjects:
                                     @"Book.png", nil];//,
    
    // Get a random asteroid file name
    int randomIndex = rand() % [asteroidFiles count];
    NSString *randomAsteroidFile = [asteroidFiles objectAtIndex:randomIndex];
    
    // Create asteroid
    Asteroid *asteroid = [[Asteroid alloc] initWithFile:randomAsteroidFile andSpeed:gameSpeed andDirection:ccp(0,-1)];
    asteroid.position = ccp(viewSize.width,250);
   
    // Setup asteroid physics
    asteroid.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){CGPointZero, asteroid.contentSize} cornerRadius:0];
    asteroid.physicsBody.collisionGroup = @"asteroidGroup"; // Don't collide w/ fellow asteroids
    asteroid.physicsBody.collisionType = @"asteroidCollision"; // When collision event fires, this will be the identifier
    
    [_physicsWorld addChild:asteroid];

}

-(void)spawnAsteroidTwo
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];

    NSMutableArray *asteroidFiles = [NSMutableArray arrayWithObjects:
                                     @"Book.png", nil];

    int randomIndex = rand() % [asteroidFiles count];
    NSString *randomAsteroidFile = [asteroidFiles objectAtIndex:randomIndex];
    
    // Create asteroid
    Asteroid *asteroidTwo = [[Asteroid alloc] initWithFile:randomAsteroidFile andSpeed:gameSpeed andDirection:ccp(0,-1)];
    asteroidTwo.position = ccp (viewSize.width,50);
    
    
    asteroidTwo.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){CGPointZero, asteroidTwo.contentSize} cornerRadius:0];
    asteroidTwo.physicsBody.collisionGroup = @"asteroidGroup"; // Don't collide w/ fellow asteroids
    asteroidTwo.physicsBody.collisionType = @"asteroidCollision"; // When collision event fires, this will be the identifier

    [_physicsWorld addChild:asteroidTwo];

    
}
@end