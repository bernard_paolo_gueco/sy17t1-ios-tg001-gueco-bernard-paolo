

#import <Foundation/Foundation.h>
#import "Player.h"

@implementation Player

- (id)initWithFile:(NSString *)fileName andSpeed:(float)speed
{
    self = [super initWithImageNamed:fileName]; // Super important
    // If you don't this you will ot recieve touches and other events
    if (self) {
        self.speed = speed;
    }
    return self;
}
- (void)update:(CCTime)delta
{
    if (!self.isMoving) return;

    float deltaSpeed = [self speed] * delta;

    CGPoint translation = ccpMult(direction, deltaSpeed);

    [self setPosition:ccpAdd(self.position, translation)];
}

- (void)moveTo:(CGPoint)point
{
    direction = ccpSub(point, self.position);
    direction = ccpNormalize(direction);
}
@end
