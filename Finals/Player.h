

#pragma once

@interface Player : CCSprite
{
    CGPoint direction;
}

- (id)initWithFile:(NSString*)fileName andSpeed:(float)speed;
- (void)moveTo:(CGPoint)point;

@property (nonatomic, readonly) int life;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) bool isMoving;

@end
