#pragma once
#import "Player.h"
#import "Asteroid.h"

@interface MainScene : CCScene <CCPhysicsCollisionDelegate> //VERY IMPORTANT FOR PHYSICS

@property (nonatomic, strong) Player *player;

@end
