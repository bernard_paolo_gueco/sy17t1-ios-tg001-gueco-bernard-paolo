
#import <Foundation/Foundation.h>
#import "Player.h"

@implementation Player

- (id)initWithFile:(NSString *)fileName andSpeed:(float)speed
{
    self = [super initWithImageNamed:fileName]; // Super important
    // If you don't this you will ot recieve touches and other events
    if (self) {
        self.speed = speed;
    }
    return self;
}

/////////////////////////////////////
// Version1 : Moving w/ delta time //
/////////////////////////////////////
- (void)update:(CCTime)delta
{
    if (!self.isMoving) return;

    float deltaSpeed = [self speed] * delta;

   CGPoint translation = ccpMult(direction, deltaSpeed);
    direction.y = 0;

    [self setPosition:ccpAdd(self.position, translation)];
}

// Version1 : Moving w/ delta time
- (void)moveTo:(CGPoint)point
{
    direction = ccpSub(point, self.position);
    direction = ccpNormalize(direction);
    
}
/////////////////////////////////////

// Version2 : Moving w/ move to
/*- (void)moveTo:(CGPoint)point
{
    // To avoid queuing actions, stop all previous actions
    [self stopAllActions];
    
    // To ensure that we travel at the same speed to every location
    // We compute the time it takes to travel by using the formula time = distance / speed
    float distance = ccpDistance(point, self.position);
    float timeToTravel = distance / self.speed;
    
    CCAction* moveAction = [CCActionMoveTo actionWithDuration:timeToTravel position:point];
    
    [self runAction:moveAction];
    
    // Rotate towards the location we touched
    // Convert the Vector2 facing direction to an angle
    // The -90.0f is to offset for north facing
    CGPoint facingDirection = ccpNormalize(ccpSub(point, self.position));
    float angle = 90.0f - CC_RADIANS_TO_DEGREES(atan2(facingDirection.y, facingDirection.x));
}*/
@end
