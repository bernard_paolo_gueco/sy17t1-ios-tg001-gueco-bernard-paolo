

#ifndef Asteroid_h
#define Asteroid_h

#import <Foundation/Foundation.h>

@interface Asteroid : CCSprite
{
    float timeElapsedA;
}

-(id)initWithFile:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction;

@property (nonatomic, assign) CGPoint direction;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) float spawnIntervalA;

@end


#endif /* Asteroid_h */
