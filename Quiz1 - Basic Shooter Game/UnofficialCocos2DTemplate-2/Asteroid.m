//
//  Asteroid.m
//  UnofficialCocos2DTemplate.git
//
//  Created by TAFT-CL4&5 on 9/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Asteroid.h"
@implementation Asteroid

int aSwitch = 30;

-(id)initWithFile:(NSString*)fileName andSpeed:(float)speed andDirection:(CGPoint)direction
{
    self = [super initWithImageNamed:fileName];
    if (self) {
        self.direction = direction;
        self.speed = speed;
     
    }
    return self;
}

-(void)update:(CCTime)delta
{
    timeElapsedA += delta;
    if (timeElapsedA >= self.spawnIntervalA)
    {
        aSwitch *= -1;
    }
    
    
    
    
      float deltaSpeed = [self speed] * delta;
      self.direction = ccpNormalize([self direction]);
    
      CGPoint translationVector = ccp(self.direction.x * deltaSpeed * aSwitch, self.direction.y * deltaSpeed);
    
      [self setPosition:ccp(self.position.x + translationVector.x, self.position.y + translationVector.y)];
    
    if (timeElapsedA >= 8)
    {
        [self removeFromParent];
    }
}

@end

