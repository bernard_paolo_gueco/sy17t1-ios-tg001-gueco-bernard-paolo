#import "MainScene.h"
//#import "Asteroid.h"

@implementation MainScene

//int asteroidSwitch = 100;

-(instancetype)init
{
    if((self = [super init]))
    {
        // IMPORTANT: Enable touches
        self.userInteractionEnabled = true;
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"WalkSample.plist"];
        
        // Collect all animation frames in an array
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        
        // Add animation frames to array
        for (int i = 1; i <= 8; i++)
        {
            NSString *fileName = [NSString stringWithFormat:@"000%d.png", i];
            CCSpriteFrame *frame = [CCSpriteFrame frameWithImageNamed:fileName];
            [walkAnimFrames addObject:frame];
        }
        
        // Add all animation frames to CCAnimation
        CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.1f];
        
        // Create a sprite
        CCSprite *walkGuy = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"0001.png"]];
        
        // Create animation action
        CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:walkAnim];
        // Repeat animation forever
        CCActionRepeatForever *repeatAnimation = [CCActionRepeatForever actionWithAction:animationAction];
        
        CCSprite *backGround = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"background.png"]];
        
        
        backGround.position = ccp(100,250);
        [walkGuy runAction:repeatAnimation];
        walkGuy.position = ccp(200,150);
        
        [self addChild:backGround];
        [self addChild:walkGuy];
    }
    
    return self;
}

-(void)update:(CCTime)delta
{
    
    
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

@end