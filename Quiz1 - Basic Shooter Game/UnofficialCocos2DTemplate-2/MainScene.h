#pragma once
#import "Player.h"

@interface MainScene : CCScene
{
    float _timeElapsed;

}

@property (nonatomic, strong) Player *player;
@property (nonatomic, assign) float spawnInterval;

@end
